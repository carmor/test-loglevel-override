﻿using Serilog.Configuration;
using Serilog;
using Serilog.Events;

namespace ChangeLogLevelTest;
internal static class LoggerSinkConfigurationExtensions
{
    public static LoggerConfiguration ConsoleErrorToWarning(this LoggerSinkConfiguration loggerSinkConfiguration)
    {
        return LoggerSinkConfiguration.Wrap(
            loggerSinkConfiguration,
            wrapped => new ErrorToWarningLogEventSinkWrapper(wrapped),
            config => config.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level} {SourceContext}] {Message:lj}{NewLine}{Exception}"),
            LogEventLevel.Verbose,
            null);
    }
}
