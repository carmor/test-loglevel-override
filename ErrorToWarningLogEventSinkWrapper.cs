﻿using Serilog.Core;
using Serilog.Events;

namespace ChangeLogLevelTest;
public class ErrorToWarningLogEventSinkWrapper : ILogEventSink, IDisposable
{
    private readonly ILogEventSink _wrappedSink;

    public ErrorToWarningLogEventSinkWrapper(ILogEventSink wrappedSink)
    {
        _wrappedSink = wrappedSink;
    }

    public void Emit(LogEvent logEvent)
    {
        var log = logEvent;
        if (logEvent.Properties.ContainsKey("SourceContext")
            && logEvent.Properties["SourceContext"].ToString().Contains("Serilog")
            && logEvent.Level == LogEventLevel.Error)
        {
            log = new LogEvent(
                log.Timestamp,
                LogEventLevel.Warning,
                log.Exception,
                log.MessageTemplate,
                log.Properties.Select(prop => new LogEventProperty(prop.Key, prop.Value)));
        }

        _wrappedSink.Emit(log);
    }
    public void Dispose()
    {
        (_wrappedSink as IDisposable)?.Dispose();
    }
}
