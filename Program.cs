﻿using ChangeLogLevelTest;
using Serilog;
using Serilog.Core;

using var log = new LoggerConfiguration()
    .WriteTo.ConsoleErrorToWarning()
    .CreateLogger();
Log.Logger = log;

var logger = Log.ForContext<ILogEventSink>();

logger.Information("Hello, Serilog!");
logger.Error("Fake Serilog Error!");
logger.Warning("Goodbye, Serilog.");

var logger2 = Log.ForContext<Program>();

logger2.Information("Hello, NOT Serilog!");
logger2.Error("Fake NOT Serilog Error!");
logger2.Warning("Goodbye, NOT Serilog.");
